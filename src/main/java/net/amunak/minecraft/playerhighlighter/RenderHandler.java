package net.amunak.minecraft.playerhighlighter;

import me.x150.renderer.event.EventListener;
import me.x150.renderer.event.EventType;
import me.x150.renderer.event.Shift;
import me.x150.renderer.event.events.RenderEvent;
import me.x150.renderer.renderer.MSAAFramebuffer;
import me.x150.renderer.renderer.RenderAction;
import me.x150.renderer.renderer.RenderActionBatch;
import me.x150.renderer.renderer.Renderer3d;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.network.ClientPlayerEntity;
import net.minecraft.client.render.Camera;
import net.minecraft.client.world.ClientWorld;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.hit.HitResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.RaycastContext;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Set;
import java.util.TreeSet;

public class RenderHandler {
	public boolean drawLines = true;

	@EventListener(shift = Shift.POST, value = EventType.WORLD_RENDER)
	void HandleRenderEvent(RenderEvent event) {
		if (!drawLines) {
			return;
		}

		MinecraftClient client = MinecraftClient.getInstance();
		ClientPlayerEntity player = client.player;
		if (player == null) {
			return;
		}

		ClientWorld world = player.clientWorld;
		Camera camera = MinecraftClient.getInstance().gameRenderer.getCamera();
		Configuration configuration = PlayerHighlighter.configuration;

		Vec3d start;
		if (!camera.isThirdPerson()) {
			double drawBeforeCameraDist = configuration.cameraDistance;
			double pitch = ((player.getPitch() + 90) * Math.PI) / 180;
			double yaw = ((player.getHeadYaw() + 90) * Math.PI) / 180;

			Vec3d modif = new Vec3d(
				Math.sin(pitch) * Math.cos(yaw) * drawBeforeCameraDist,
				Math.cos(pitch) * drawBeforeCameraDist - 0.35,
				Math.sin(pitch) * Math.sin(yaw) * drawBeforeCameraDist
			);

			start = camera.getPos().add(modif);
		} else {
			start = player.getEyePos();
		}

		Set<EntityDatum> entities = new TreeSet<>((o1, o2) -> Float.compare(o1.distance(), o2.distance()));

		ENTITYLOOP: for (Entity entity : world.getEntities()) {
			if (!(entity instanceof LivingEntity)) {
				continue;
			}

			String entityCategory = EntityHelper.getEntityCategory(entity);
			if (entityCategory == null) {
				continue;
			}

			Configuration.EntityGroup configCategory = configuration.getEntityConfiguration(entityCategory);
			if (!configCategory.isEnabled()) {
				continue;
			}

			// sneak check
			if (!configCategory.showSneaking && entity.isSneaky()) {
				continue;
			}

			// visibility check
			if (!configCategory.showInvisible && entity.isInvisibleTo(player)) {
				continue;
			}

			// distance check
			float distance = entity.distanceTo(player);
			float maxDistance = configCategory.maxDistance;
			if (distance > maxDistance) {
				continue;
			}

			// vertical distance check
			// the 1.0 is a correcting factor for the fact that players count from their upper half (or something)
			// it's more than 1 actually (probably eyeHeight) but this works well as you generally want to see more stuff
			// above you than below anyway... And who cares about half-block heights and such.
			float vertDistance = Math.abs((float) ((player.getY() + 1.0) - entity.getY()));
			float maxVertDistance = configCategory.maxVerticalDistance;
			if (vertDistance > maxVertDistance) {
				continue;
			}

			// do raycasting to check actual visibility
			if (configCategory.respectLineOfSight) {
				float tickDelta = client.getLastFrameDuration();
				Vec3d source = player.getCameraPosVec(tickDelta);
				Vec3d target = entity.getCameraPosVec(tickDelta);
				while (true) {
					BlockHitResult hitResult = world.raycast(new RaycastContext(source, target, RaycastContext.ShapeType.OUTLINE, RaycastContext.FluidHandling.NONE, entity));

					if (new BlockPos(target).equals(hitResult.getBlockPos())) {
						// has LoS
						break;
					}

					// ignore translucent blocks
//					if (hitResult.getType() == HitResult.Type.BLOCK && world.getBlockState(hitResult.getBlockPos()).isTranslucent(world, cameraBlockPos)) {
//						// move to last translucent block and continue
//						// causes infinite loop - needs to move forward by 1 block to not keep checking
//						source = Vec3d.ofCenter(hitResult.getBlockPos());
//						continue;
//					}

					// not a translucent block, skip entity
					continue ENTITYLOOP;
				}
			}

			entities.add(new EntityDatum((LivingEntity) entity, configCategory, distance));
		}

		ArrayList<RenderAction> batch = new ArrayList<>();
		MSAAFramebuffer.use(MSAAFramebuffer.MAX_SAMPLES, () -> {
			// main draw loop
			int drawn = 0;
			for (EntityDatum datum : entities) {
				// calculate alpha based on distance - the further the more transparent
				float alpha = 1 - (datum.distance() / (datum.configCategory().maxDistance / 2F));

				// clamp alpha
				alpha = Math.max(0.2F, Math.min(1.0F, alpha));

				Color color = EntityHelper.getRenderColor(datum.entity(), datum.configCategory(), alpha);
				batch.add(Renderer3d.renderLine(start, datum.entity().getEyePos(), new me.x150.renderer.renderer.color.Color(color)));

				drawn++;
				if (drawn >= configuration.maxLines) {
					break;
				}
			}
		});

		Renderer3d.startRenderingThroughWalls();
		new RenderActionBatch(batch.toArray(RenderAction[]::new)).drawAllWithoutVbo(event.getStack());

		entities.clear();
	}

	public record EntityDatum(LivingEntity entity, Configuration.EntityGroup configCategory, float distance) {}
}
