package net.amunak.minecraft.playerhighlighter;

import me.lortseam.completeconfig.api.ConfigContainer;
import me.lortseam.completeconfig.api.ConfigEntries;
import me.lortseam.completeconfig.api.ConfigEntry;
import me.lortseam.completeconfig.api.ConfigGroup;
import me.lortseam.completeconfig.data.Config;
import me.shedaniel.math.Color;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public class Configuration extends Config {
	public Configuration() {
		super(PlayerHighlighter.MOD_ID);
	}

	Map<String, EntityGroup> entityConfiguration = new LinkedHashMap<>();

	@ConfigEntry
	private boolean enabled = true;

	@ConfigEntry
	@ConfigEntry.BoundedFloat(min = -10F, max = 30F)
//	@ConfigEntry.Slider
	public float cameraDistance = 1.0F;

	@ConfigEntry
	@ConfigEntry.BoundedInteger(min = 1, max = 255)
	@ConfigEntry.Slider
	public int maxLines = 10;

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;

		PlayerHighlighter.renderHandler.drawLines = enabled;
	}

	public EntityGroup getEntityConfiguration(String category) {
		EntityGroup config = this.entityConfiguration.get(category);

		if (config == null) {
			PlayerHighlighter.LOGGER.error("Accessing invalid config section '" + category + "'");
			return new EntityGroup("invalid");
		}

		return config;
	}

	@Override
	public @Nullable Collection<ConfigContainer> getTransitives() {
		for (String category : EntityCategory.ENTITY_CATEGORIES) {

			this.entityConfiguration.put(category, category.equals(EntityCategory.CATEGORY_ENTITIES_PLAYERS) ? new PlayerEntityGroup(category) : new EntityGroup(category));
		}

		return new ArrayList<>(entityConfiguration.values());
	}

	static class EntityGroup implements ConfigGroup {
		public String entityCategory;

		@ConfigEntry(nameKey = "entities.enabled")
		private boolean enabled = true;

		@ConfigEntry(nameKey = "entities.showInvisible")
		public boolean showInvisible;

		@ConfigEntry(nameKey = "entities.showSneaking")
		public boolean showSneaking = true;

		@ConfigEntry(nameKey = "entities.maxDistance", descriptionKey = "entities.maxDistance.description")
		@ConfigEntry.BoundedInteger(min = 1, max = 1024)
		@ConfigEntry.Slider
		public int maxDistance = 64;

		@ConfigEntry(nameKey = "entities.maxVerticalDistance", descriptionKey = "entities.maxVerticalDistance.description")
		@ConfigEntry.BoundedInteger(min = 1, max = 512)
		@ConfigEntry.Slider
		public int maxVerticalDistance;

		@ConfigEntry(nameKey = "entities.color", descriptionKey = "entities.color.description")
		@ConfigEntry.Color(alphaMode = false)
		public Color color;

		@ConfigEntry(nameKey = "entities.respectLineOfSight")
		public boolean respectLineOfSight = false;

		public EntityGroup(String entityCategory) {
			this.entityCategory = entityCategory;

			// Players are more important and you probably want to see them further away (especially since they can fly)
			this.maxVerticalDistance = entityCategory.equals(EntityCategory.CATEGORY_ENTITIES_PLAYERS) ? 36 : 12;

			// Other entities include armor stands and you almost definitely don't want to see invisible ones
			this.showInvisible = !entityCategory.equals(EntityCategory.CATEGORY_ENTITIES_OTHER);

			switch (entityCategory) {
				case (EntityCategory.CATEGORY_ENTITIES_PLAYERS) -> this.color = Color.ofRGB(255, 0, 255);
				case (EntityCategory.CATEGORY_ENTITIES_CREATURES) -> this.color = Color.ofRGB(0, 255, 0);
				case (EntityCategory.CATEGORY_ENTITIES_MONSTERS) -> this.color = Color.ofRGB(0, 255, 255);
				case (EntityCategory.CATEGORY_ENTITIES_OTHER)  -> this.color = Color.ofRGB(255, 255, 0);
				default -> this.color = Color.ofRGB(255, 128, 64);
			}
		}

		@Override
		public String getId() {
			return entityCategory;
		}

		public boolean isEnabled() {
			return enabled;
		}

		public void setEnabled(boolean enabled) {
			this.enabled = enabled;
		}
	}

	static class PlayerEntityGroup extends EntityGroup {
		@ConfigEntry
		public boolean ignoreFake = true;

		@ConfigEntry
		public boolean respectTeamColors = true;

		@ConfigEntry
		public boolean showOwnTeam = true;

		@ConfigEntry
		public boolean enableOwnTeamCustomColor = false;


		@ConfigEntry
		@ConfigEntry.Color(alphaMode = false)
		public Color ownTeamCustomColor = Color.ofRGB(255, 255, 255);

		@ConfigEntry
		public boolean showEnemyTeam = true;

		@ConfigEntry
		public boolean enableEnemyTeamCustomColor = false;

		@ConfigEntry
		@ConfigEntry.Color(alphaMode = false)
		public Color enemyTeamCustomColor = Color.ofRGB(255, 0, 0);

		@ConfigEntry
		public boolean showNonTeamPlayers = true;

		public PlayerEntityGroup(String entityCategory) {
			super(entityCategory);
		}

		@Override
		public boolean isEnabled() {
			if (!super.isEnabled()) {
				return false;
			}

			return this.showOwnTeam || this.showEnemyTeam || this.showNonTeamPlayers;
		}
	}
}
