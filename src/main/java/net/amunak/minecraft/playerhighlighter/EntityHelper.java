package net.amunak.minecraft.playerhighlighter;

import net.minecraft.client.MinecraftClient;
import net.minecraft.client.network.ClientPlayNetworkHandler;
import net.minecraft.client.network.ClientPlayerEntity;
import net.minecraft.client.network.OtherClientPlayerEntity;
import net.minecraft.client.network.PlayerListEntry;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.mob.AmbientEntity;
import net.minecraft.entity.mob.Monster;
import net.minecraft.entity.mob.WaterCreatureEntity;
import net.minecraft.entity.passive.PassiveEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.scoreboard.AbstractTeam;
import net.minecraft.util.Formatting;
import org.jetbrains.annotations.NotNull;

import java.awt.Color;
import java.util.UUID;

class EntityHelper {
	static String getEntityCategory(Entity entity) {
		if (entity instanceof OtherClientPlayerEntity otherPlayerEntity) {
			if (isFakePlayer(otherPlayerEntity)) {
				return null;
			}

			Configuration.PlayerEntityGroup config = (Configuration.PlayerEntityGroup) PlayerHighlighter.configuration.getEntityConfiguration(EntityCategory.CATEGORY_ENTITIES_PLAYERS);
			AbstractTeam team = (otherPlayerEntity).getScoreboardTeam();
			ClientPlayerEntity player = MinecraftClient.getInstance().player;
			if (team != null && player != null) {

				boolean sameTeam = entity.isTeammate(player);
				if (sameTeam && !config.showOwnTeam) {
					return null;
				}
				if (!sameTeam && !config.showEnemyTeam) {
					return null;
				}
			}

			if (team == null && !config.showNonTeamPlayers) {
				return null;
			}

			return EntityCategory.CATEGORY_ENTITIES_PLAYERS;
		}

		// other players - like *the* player - should be ignored
		if (entity instanceof PlayerEntity) {
			return null;
		}

		if (entity instanceof LivingEntity living) {
			// nobody cares about ambient creatures - hopefully
			if (entity instanceof AmbientEntity) {
				return null;
			}

			if (living instanceof Monster) {
				return EntityCategory.CATEGORY_ENTITIES_MONSTERS;
			}

			if (living instanceof PassiveEntity || living instanceof WaterCreatureEntity) {
				return EntityCategory.CATEGORY_ENTITIES_CREATURES;
			}

			return EntityCategory.CATEGORY_ENTITIES_OTHER;
		}

		return null;
	}

	private static boolean isFakePlayer(OtherClientPlayerEntity entity) {
		// do we even want to check?
		Configuration.PlayerEntityGroup entityConfiguration = (Configuration.PlayerEntityGroup) PlayerHighlighter.configuration.getEntityConfiguration(EntityCategory.CATEGORY_ENTITIES_PLAYERS);
		if (!entityConfiguration.ignoreFake) {
			return false;
		}

		UUID id = entity.getGameProfile().getId();
		if (id == null) {
			// definitely fake
			return true;
		}

		ClientPlayNetworkHandler networkHandler = MinecraftClient.getInstance().getNetworkHandler();
		// we don't have connection but have "other player", therefore it *must be* fake
		// this is a weird edge case that shouldn't really happen or matter
		if (networkHandler == null || !networkHandler.getConnection().isOpen()) {
			return true;
		}

		// assume player is fake only if they have UUID and are in player list
		for (PlayerListEntry playerInfo : networkHandler.getPlayerList()) {
			if (id.equals(playerInfo.getProfile().getId())) {
				return false;
			}
		}

		return true;
	}

	static Color getRenderColor(Entity entity, Configuration.EntityGroup configCategory, float alpha) {

		AbstractTeam team = entity.getScoreboardTeam();
		if (team != null && configCategory instanceof Configuration.PlayerEntityGroup playerConfigCategory) {
			// handle color overrides
			if (playerConfigCategory.enableOwnTeamCustomColor || playerConfigCategory.enableEnemyTeamCustomColor) {
				ClientPlayerEntity player = MinecraftClient.getInstance().player;
				boolean teammate = entity.isTeammate(player);
				if (teammate && playerConfigCategory.enableOwnTeamCustomColor) {
					return configColorToColor(playerConfigCategory.ownTeamCustomColor, alpha);
				}
				if (!teammate && playerConfigCategory.enableEnemyTeamCustomColor) {
					return configColorToColor(playerConfigCategory.enemyTeamCustomColor, alpha);
				}
			}

			if (playerConfigCategory.respectTeamColors) {
				Formatting teamColor = team.getColor();
				if (teamColor.isColor()) {
					Integer colorValue = teamColor.getColorValue();
					if (colorValue != null) {
						return colorValueToColor(colorValue);
					}
				}
			}
		}

		return configColorToColor(configCategory.color, alpha);
	}

	@NotNull
	private static Color colorValueToColor(Integer colorValue) {
		return new Color((float) (colorValue >> 16) / 255.0F, (float) (colorValue >> 8 & 255) / 255.0F, (float) (colorValue & 255) / 255.0F);
	}

	@NotNull
	static Color configColorToColor(me.shedaniel.math.Color color, float alpha) {
		return new Color(color.getRed(), color.getGreen(), color.getBlue(), (int) (alpha * 255));
	}
}
