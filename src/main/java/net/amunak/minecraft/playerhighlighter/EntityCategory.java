package net.amunak.minecraft.playerhighlighter;

public class EntityCategory {
	public static final String CATEGORY_ENTITIES_PLAYERS = "players";
	public static final String CATEGORY_ENTITIES_CREATURES = "creatures";
	public static final String CATEGORY_ENTITIES_MONSTERS = "monsters";
	public static final String CATEGORY_ENTITIES_OTHER = "other";

	public static final String[] ENTITY_CATEGORIES = {
			CATEGORY_ENTITIES_PLAYERS,
			CATEGORY_ENTITIES_CREATURES,
			CATEGORY_ENTITIES_MONSTERS,
			CATEGORY_ENTITIES_OTHER,
	};
}
