package net.amunak.minecraft.playerhighlighter;

import me.lortseam.completeconfig.gui.ConfigScreenBuilder;
import me.lortseam.completeconfig.gui.cloth.ClothConfigScreenBuilder;
import me.x150.renderer.event.Events;
import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.fabric.api.client.event.lifecycle.v1.ClientTickEvents;
import net.fabricmc.loader.api.FabricLoader;
import net.minecraft.client.option.KeyBinding;
import net.minecraft.client.util.InputUtil;
import net.minecraft.text.Text;
import org.lwjgl.glfw.GLFW;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import net.fabricmc.fabric.api.client.keybinding.v1.KeyBindingHelper;

public class PlayerHighlighter implements ClientModInitializer {
	public static final String MOD_ID = "playerhighlighter";
	public static final Logger LOGGER = LoggerFactory.getLogger("playerhighlighter");
	static Configuration configuration;
	public static RenderHandler renderHandler;

	@Override
	public void onInitializeClient() {
		configuration = new Configuration();
		configuration.load();

		if (FabricLoader.getInstance().isModLoaded("cloth-config")) {
			ConfigScreenBuilder.setMain(MOD_ID, new ClothConfigScreenBuilder());
		}

		renderHandler = new RenderHandler();
		renderHandler.drawLines = configuration.isEnabled();
		Events.registerEventHandlerClass(renderHandler);

		registerKeyBindings();
	}

	private void registerKeyBindings() {
		KeyBinding kb = KeyBindingHelper.registerKeyBinding(new KeyBinding(
				"key.playerhighlighter.toggle",
				InputUtil.Type.KEYSYM,
				GLFW.GLFW_KEY_KP_0,
				KeyBinding.MISC_CATEGORY
		));
		ClientTickEvents.END_CLIENT_TICK.register(client -> {
			while (kb.wasPressed()) {
				renderHandler.drawLines = !renderHandler.drawLines;

				if (client.player != null) {
					if (renderHandler.drawLines) {
						client.player.sendMessage(Text.translatable("message.playerhighlighter.toggle.enable"), true);
					} else {
						client.player.sendMessage(Text.translatable("message.playerhighlighter.toggle.disable"), true);
					}
				}
			}
		});
	}
}
